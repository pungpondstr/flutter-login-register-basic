import 'package:flutter/material.dart';

class Register extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: Container(
        margin: EdgeInsets.only(left: 20, right: 20),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            FlutterLogo(
              size: 200,
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Firstname'),
              style: TextStyle(height: 2, fontSize: 18),
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Lastname'),
              style: TextStyle(height: 2, fontSize: 18),
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Email'),
              style: TextStyle(height: 2, fontSize: 18),
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Password'),
              style: TextStyle(height: 2, fontSize: 18),
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Confirm password'),
              style: TextStyle(height: 2, fontSize: 18),
            ),
            TextField(
              decoration: InputDecoration(hintText: 'Tel'),
              style: TextStyle(height: 2, fontSize: 18),
            ),
            Container(
              margin: EdgeInsets.only(top: 15),
              child: RaisedButton(
                padding: EdgeInsets.only(top: 15, bottom: 15),
                onPressed: () => print('welcome'),
                color: Colors.blue,
                textColor: Colors.white,
                child: Center(
                  child: Text(
                    'Register',
                    style: TextStyle(fontSize: 18),
                  ),
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
